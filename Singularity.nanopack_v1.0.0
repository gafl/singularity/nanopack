Bootstrap:docker
From:debian:9.5-slim


%environment
LC_ALL=C
export LC_ALL
LC_NUMERIC=en_GB.UTF-8
export LC_NUMERIC

%help
Container with ONT QC tools:
See: https://github.com/wdecoster/nanopack
version: 1.0.0

Content
Scripts
NanoPlot: creating many relevant plots derived from reads (fastq), alignments (bam) and albacore summary files. Examples can be found in the gallery on my blog. NanoPack is also available with a graphical user interface in NanoGUI or as a web service.
NanoComp: comparing multiple runs on read length and quality based on reads (fastq), alignments (bam) or albacore summary files.
NanoQC: Generating plots to investigate nucleotide composition and quality distribution at the end of reads.
NanoStat: Quickly create a statistical summary from reads, an alignment or a summary file.
NanoFilt: Streaming script for filtering a fastq file based on a minimum length, minimum quality cut-off, minimum and maximum average GC. Also trimming nucleotides from either read ends is an option.
NanoLyse: Streaming script for filtering a fastq file to remove reads mapping to the lambda phage genome (control DNA used in nanopore sequencing). Uses minimap2/mappy.

Modules
nanoget: Functions for extracting features from reads, alignments and albacore summary data, parallelized.
nanomath: Functions for mathematical processing and calculating statistics.
nanoplotter: Appropriate plotting functions, building on the seaborn module.


Usage example:
singularity exec nanopack_v1.0.0.simg NanoPlot -h
singularity exec nanopack_v1.0.0.simg NanoComp -h
singularity exec nanopack_v1.0.0.simg NanoStat -h



%post
export PYTHONIOENCODING=UTF-8

        apt-get update
        apt-get upgrade -y
        apt-get install -y git build-essential wget unzip libz-dev python3-dev python3-pip libbz2-dev liblzma-dev

	pip3 install nanopack --upgrade

	echo "Container with ONT QC tools:" >/usage.txt
	echo "See: https://github.com/wdecoster/nanopack" >>/usage.txt
	echo "Content" >>/usage.txt
	echo "Scripts" >>/usage.txt
	echo "NanoPlot: creating many relevant plots derived from reads (fastq), alignments (bam) and albacore summary files. Examples can be found in the gallery on my blog. NanoPack is also available with a graphical user interface in NanoGUI or as a web service." >>/usage.txt
	echo "NanoComp: comparing multiple runs on read length and quality based on reads (fastq), alignments (bam) or albacore summary files." >>/usage.txt
	echo "NanoQC: Generating plots to investigate nucleotide composition and quality distribution at the end of reads." >>/usage.txt
	echo "NanoStat: Quickly create a statistical summary from reads, an alignment or a summary file." >>/usage.txt
	echo "NanoFilt: Streaming script for filtering a fastq file based on a minimum length, minimum quality cut-off, minimum and maximum average GC. Also trimming nucleotides from either read ends is an option." >>/usage.txt
	echo "NanoLyse: Streaming script for filtering a fastq file to remove reads mapping to the lambda phage genome (control DNA used in nanopore sequencing). Uses minimap2/mappy." >>/usage.txt
	echo "" >>/usage.txt
	echo "Modules" >>/usage.txt
	echo "nanoget: Functions for extracting features from reads, alignments and albacore summary data, parallelized." >>/usage.txt
	echo "nanomath: Functions for mathematical processing and calculating statistics." >>/usage.txt
	echo "nanoplotter: Appropriate plotting functions, building on the seaborn module." >>/usage.txt
	echo "" >>/usage.txt
	echo "" >>/usage.txt
	echo "Usage example:" >>/usage.txt
	echo "singularity exec nanopack_v1.0.0.simg NanoPlot -h" >>/usage.txt
	echo "singularity exec nanopack_v1.0.0.simg NanoComp -h" >>/usage.txt
	echo "singularity exec nanopack_v1.0.0.simg NanoStat -h" >>/usage.txt

	
	#Clean up
	apt-get clean
	apt-get autoclean

%runscript
	exec cat /usage.txt

